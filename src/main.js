import 'reset-css'
import 'line-awesome/dist/line-awesome/css/line-awesome.css'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import i18n from './i18n'
import PortalVue from 'portal-vue'
import * as Sentry from '@sentry/browser'
import { Vue as VueIntegration } from '@sentry/integrations'
import VueGtag from 'vue-gtag'

// eslint-disable-next-line no-console
console.log(`%cBard v${import.meta.env.VITE_APP_VERSION}`, 'font-size: 40px')

Sentry.init({
  dsn: import.meta.env.VITE_APP_SENTRY_DSN,
  integrations: [new VueIntegration({ Vue, attachProps: true })]
})

Vue.use(PortalVue)
Vue.use(VueGtag, {
  config: { id: import.meta.env.VITE_APP_GTAG }
})
Vue.config.productionTip = false

new Vue({
  i18n,
  router,
  render: h => h(App)
}).$mount('#app')
