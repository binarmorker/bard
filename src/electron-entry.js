const { app, ipcMain, BrowserWindow, Menu } = require('electron')
const express = require('express')
const cors = require('cors')
const multer = require('multer')
const path = require('path')

let mainWindow, loadingWindow, server
const icon = path.join(__dirname, '/img/icons/png/32x32.png')
const url = process.env.SERVE ? 'http://localhost:8080' : path.join(__dirname, '/index.html')
const windowOptions = {
  show: false,
  width: 1025,
  height: 769,
  frame: false
}
const webPreferences = {
  contextIsolation: false,
  nodeIntegration: true,
  enableRemoteModule: false,
  enableWebSQL: false
}

const initializeServer = () => new Promise(resolve => {
  server = express()
  server.use(cors({ origin: 'http://localhost:8080' }))
  server.post('/config/song', multer({ dest: '.bard-config/song/' }).single('song'), (req, res) => {
    res.json(req.file)
  })
  server.use('/config', express.static('.bard-config'))
  server.listen(4500, () => {
    resolve()
  })
})

const createLoadingWindow = () => {
  loadingWindow = new BrowserWindow({
    ...windowOptions,
    icon,
    webPreferences
  })

  loadingWindow.setResizable(false)
  loadingWindow.loadFile(path.join(__dirname, '/loading.html'))

  loadingWindow.webContents.on('did-finish-load', () => {
    loadingWindow.show()
  })

  loadingWindow.on('closed', () => {
    loadingWindow = null
  })
}

const createWindow = () => new Promise(resolve => {
  mainWindow = new BrowserWindow({
    ...windowOptions,
    icon,
    webPreferences: {
      ...webPreferences,
      preload: path.join(__dirname, '/electron-preload.js')
    }
  })

  mainWindow.loadURL(url)

  mainWindow.on('ready-to-show', resolve)

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  if (process.env.SERVE) {
    mainWindow.webContents.on('before-input-event', (event, input) => {
      if (input.control && input.shift && input.key.toLowerCase() === 'i') {
        console.log('Pressed Control+Shift+I')
        mainWindow.webContents.openDevTools()
        event.preventDefault()
      }

      if (input.control && input.key.toLowerCase() === 'r') {
        console.log('Pressed Control+R')
        mainWindow.webContents.reload()
        event.preventDefault()
      }
    })
  }
})

app.on('ready', () => {
  createLoadingWindow()
  Promise.allSettled([
    createWindow(),
    initializeServer()
  ]).then(() => {
    if (loadingWindow) {
      loadingWindow.hide()
      loadingWindow.close()
    }

    mainWindow.show()
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

ipcMain.on('minimize', () => {
  mainWindow.minimize()
})

ipcMain.on('maximize', () => {
  if (mainWindow.isMaximized()) {
    mainWindow.unmaximize()
  } else {
    mainWindow.maximize()
  }
})

ipcMain.on('close', () => {
  mainWindow.close()
})

Menu.setApplicationMenu(null)
