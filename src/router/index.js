import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import Song from '@/views/Song.vue'
import NotFound from '@/views/NotFound.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/song',
    name: 'Song',
    component: Song
  },
  {
    path: '/notfound',
    name: 'Not Found',
    component: NotFound,
    meta: {
      ignoreAuthentication: true
    }
  },
  {
    path: '/*',
    redirect: {
      name: 'Not Found'
    }
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: import.meta.env.BASE_URL,
  routes
})

export default router
