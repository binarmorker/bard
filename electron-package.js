const fs = require('fs')

console.log('Moving electron files to dist folder')
fs.copyFileSync('./src/electron-entry.js', './dist/electron-entry.js')
fs.copyFileSync('./src/electron-preload.js', './dist/electron-preload.js')

console.log('Generating package.json')
const packageFile = fs.readFileSync('./package.json')
const { name, version, private: _private, description, author, contributors } = JSON.parse(packageFile)
const newPackageFile = { name, version, private: _private, description, author, contributors, main: 'electron-entry.js' }
fs.writeFileSync('./dist/package.json', JSON.stringify(newPackageFile))

console.log('Removing unused files from dist folder')
