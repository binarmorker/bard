import path from 'path'
import { defineConfig } from 'vite'
import { createVuePlugin } from 'vite-plugin-vue2'
import mdPlugin, { Mode } from 'vite-plugin-markdown'

export default defineConfig({
  base: '',
  plugins: [
    createVuePlugin(),
    mdPlugin({
      mode: Mode.HTML
    })
  ],
  resolve: {
    alias: [
      {
        find: '@',
        replacement: path.resolve(__dirname, 'src')
      },
      {
        find: '~@',
        replacement: path.resolve(__dirname, 'src')
      }
    ]
  },
  server: {
    port: 8080
  }
})
